import mongoose from "mongoose"
import { databaseLink } from "../constant.js"

let connectToMongoDB = async()=>{
    try {
      await mongoose.connect(databaseLink)
      console.log("Application is connected to mongodb successfully.")
    } catch (error) {
      console.log(error.message)
    }
   
  }
  
  export default connectToMongoDB