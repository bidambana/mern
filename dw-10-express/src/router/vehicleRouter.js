import { Router } from "express";

let vehicleRouter = Router()

vehicleRouter
.route("/")
.post((req,res,next)=>{
    res.json({
        success: true,
        message: "vehicles created successfully"
    })
    
})
.get((req,res,next)=>{
    res.json({
        success: true,
        message: "vehicles read successfully"
    })
    
})
.patch((req,res,next)=>{
    res.json({
        success: true,
        message: "vehicles updated successfully"
    })
    
})
.delete((req,res,next)=>{
    res.json({
        success: true,
        message: "vehicles deleted successfully"
    })
    
})

export default vehicleRouter