import { Router } from "express";

let schoolRouter = Router()

schoolRouter
.route("/")
.post((req,res,next)=>{
    res.json({
        success: true,
        message: "schools created successfully"
    })
    
})
.get((req,res,next)=>{
    res.json({
        success: true,
        message: "schools read successfully"
    })
    
})
.patch((req,res,next)=>{
    res.json({
        success: true,
        message: "schools updated successfully"
    })
    
})
.delete((req,res,next)=>{
    res.json({
        success: true,
        message: "schools deleted successfully"
    })
    
})

export default schoolRouter