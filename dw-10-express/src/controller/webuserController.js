import { Webuser } from "../schema/model.js"


export let createWebuser = async(req,res)=>{
    let webuserData = req.body
    try {
        let result = await Webuser.create(webuserData)
        res.json({
            success: true,
            message: "webuser created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }   
}

export let readWebuser = async(req,res)=>{
    try {
        //sorting
        //let result=await Webuser.find({}).sort("name") //ascending
        //let result=await Webuser.find({}).sort("-name") // descending
        //let result=await Webuser.find({}).sort("name age") // ascending
        //let result=await Webuser.find({}).sort("name -age") // descending

        //pagination 

        //skip
        //let result=await Webuser.find({}).skip("3") //skip first three data

        //limit
        //let result = await Webuser.find({}).limit("5") //limit to 5 data

        //skip works first before limit
        //find sort select skip limit

        let result = await Webuser.find({}).select("name age -_id")
        res.json({
            success: true,
            message: "webusers read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }  
}

export let readWebuserDetails = async(req,res)=>{
    let webuserId = req.params.webuserId
    try {
        let result = await Webuser.findById(webuserId)
        res.json({
            success:true,
            message:"webuser read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateWebuser = async(req,res)=>{
    let webuserId = req.params.webuserId
    let webuserData = req.body
    try {
        let result = await Webuser.findByIdAndUpdate(webuserId,webuserData)
        res.json({
            success: true,
            message: "webuser updated successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   
    
}

export let deleteWebuser = async(req,res)=>{
    let webuserId = req.params.webuserId
    try {
        let result = await Webuser.findByIdAndDelete(webuserId)
        res.json({
            success: true,
            message: "webuser deleted successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    
    
}