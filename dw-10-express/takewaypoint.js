/*
-middleware are the function which has req, res and next
-to trigger another middleware we have to call next()
-middleware is divided into two parts:
    normal middleware
        (req,res,next)=>{}
        to trigger next middleware we have to call next() 
    error middleware
        (err,req,res,next)=>{}
        to trigger next error middleware we have to call next("a")

-in form data, we can get file information  through req.files whereas we can get other information via req.body 

.env
-always made in root folder
-in .env we define special variable (credentials, port, link)
-variable name must be in uppercase
-everything is in string 



















*/