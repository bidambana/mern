import { serverLink } from "../constant.js"

export let handleImage =(req,res)=>{
    let links =req.files.map((value,i)=>{
        return `${serverLink}${value.filename}`
    })
    res.json({
        success:true,
        message:"image uploaded successfully",
        result:links
    })
}