import { Schema } from "mongoose";

let bookSchema = Schema({
    name:{
        type:String,
        required:[true, "Name field is required"]
    },
    author:{
        type:String,
        required:[true, "Author field is required"]
    },
    price:{
        type:Number,
        required:[true,"Price field is required"]
    },
    isAvailable:{
        type:Boolean,
        required:[true, "isAvailable field is required"]
    }
})

export default bookSchema