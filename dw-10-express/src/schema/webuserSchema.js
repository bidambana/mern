import { Schema } from "mongoose";

let webuserSchema = Schema({
    name:{
        type:String,
        required:[true,"name field is required"],
        validate:(value)=>{
            let isValid=/^[a-zA-Z0-9]{2,20}$/.test(value)
            if(!isValid){
                throw new Error("Name should contain alphanumeric value and should be at least 2 characters long and 20 characters at most")
            }
        }
    },
    age:{
        type:Number,
        required:[true,"age field is required"],
        min:18,
        max:70
    },
    password:{
        type:String,
        required:[true,"password field is required"]
    },
    phoneNumber:{
        type:Number,
        required:[true,"phoneNumber field is required"],
        validate:(value)=>{
            let strValue = String(value)
            let strLen = strValue.length
            if(strLen !==10){
                throw new Error("Phone number must be exactly 10 characters long.")
            }
        }
    },
    roll:{
        type:Number,
        required:[true,"roll field is required"]
    },
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required"]
    },
    spouseName:{
        type:String,
        required:[true,"spouseName field is required"]
    },
    email:{
        type:String,
        unique:true,
        required:[true,"email field is required"],
        validate: (value) => {
            let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
              value
            );
            if (!isValid) {
              throw new Error("email not valid.");
            }
          },

    },
    gender:{
        type:String,
        required:[true,"gender field is required"],
        enum: {
            values: ["male", "female", "other"],
            message: (notEnum) => {
              return `${notEnum.value} is not valid enum`;
            },
          },
    },
    dob:{
        type:Date,
        required:[true,"dob field is required"]
    },
    location:{
        country:{
            type:String,
            required:[true,"country field is required"]
        },
        exactLocation:{
            type:String,
            required:[true,"exactLocation field is required"]
        }
    },
    favTeacher:[
        {
            type:String,
            required:[true,"favTeacher field is required"]
        }
    ],
    favSubject:[
        {
            bookName:{
                type:String,
                required:[true,"bookName field is required"]
            },
            bookAuthor:{
                type:String,
                required:[true,"bookAuthor field is required"]
            }
        }
    ]
})

export default webuserSchema