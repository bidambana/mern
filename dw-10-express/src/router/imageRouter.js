import { Router } from "express";
import upload from "../middleware/upload.js";
import { handleImage } from "../controller/imageController.js";

let imageRouter = Router();

imageRouter
  .route("/") // localhost:8000/images
  .post(upload.array("document",4),handleImage)
  


export default imageRouter;

