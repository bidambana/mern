import { Product } from "../schema/model.js"

export let createProduct = async(req,res)=>{
    let productData = req.body
    try {
        let result = await Product.create(productData)
        res.json({
            success: true,
            message: "product created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }  
}

export let readProduct = async(req,res)=>{
    try {
        let result = await Product.find({})
        res.json({
            success: true,
            message: "products read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

export let readProductDetails = async(req,res)=>{
    let productId=req.params.productId
    try {
        let result = await Product.findById(productId)
        res.json({
            success:true,
            message:"product read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateProduct = async(req,res)=>{
    let productId = req.params.productId
    let productData = req.body
    try {
        let result = await Product.findByIdAndUpdate(productId,productData)
        res.json({
            success:true,
            message:"product updated successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteProduct = async(req,res)=>{
    let productId = req.params.productId
    try {
        let result = await Product.findByIdAndDelete(productId)
        res.json({
            success:true,
            message:"product deleted successfully",
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}