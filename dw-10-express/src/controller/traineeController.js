import { Trainee } from "../schema/model.js"

export let createTrainee = async(req,res)=>{
    let traineeData = req.body
    try {
        let result = await Trainee.create(traineeData)
        res.json({
            success: true,
            message: "trainee created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }   
}

export let readTrainee = async(req,res)=>{
    try {
        let result = await Trainee.find({})
        res.json({
            success: true,
            message: "trainees read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }  
}

export let readTraineeDetails = async(req,res)=>{
    let traineeId = req.params.traineeId
    try {
        let result = await Trainee.findById(traineeId)
        res.json({
            success:true,
            message:"trainee read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateTrainee = async(req,res)=>{
    let traineeId = req.params.traineeId
    let traineeData = req.body
    try {
        let result = await Trainee.findByIdAndUpdate(traineeId,traineeData)
        res.json({
            success: true,
            message: "trainee updated successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   
    
}

export let deleteTrainee = async(req,res)=>{
    let traineeId = req.params.traineeId
    try {
        let result = await Trainee.findByIdAndDelete(traineeId)
        res.json({
            success: true,
            message: "trainee deleted successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    
    
}