// make an express application
// attach port to that application
import express, { json } from "express";
import connectToMongoDB from "./src/connectToDb/connectToMongoDb.js";
import bikeRouter from "./src/router/bikeRouter.js";
import bookRouter from "./src/router/bookRouter.js";
import collegeRouter from "./src/router/collegeRouter.js";
import firstRouter from "./src/router/firstRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import schoolRouter from "./src/router/schoolRouter.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import userRouter from "./src/router/userRouter.js";
import vehicleRouter from "./src/router/vehicleRouter.js";
import webuserRouter from "./src/router/webuserRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import imageRouter from "./src/router/imageRouter.js";
import { port } from "./src/constant.js";

let expressApp = express();
expressApp.use(json());
expressApp.use(express.static("./public"))      //to make a folder static


expressApp.listen(port, () => {
  console.log(`express application is listening at port ${port}`);
});

connectToMongoDB()

expressApp.use("/firsts",firstRouter);
expressApp.use("/bikes",bikeRouter)
expressApp.use("/trainees",traineeRouter)
expressApp.use("/schools",schoolRouter)
expressApp.use("/vehicles",vehicleRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/users",userRouter)
expressApp.use("/webusers",webuserRouter)
expressApp.use("/products",productRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/files",fileRouter)
expressApp.use("/images",imageRouter)







// let files = [
//     {
//       destination: "./public",
//       filename: "abc.jpg",
//     },
//     {
//       destination: "./public",
//       filename: "nitan.jpg",
//     },
//     {
//       destination: "./public",
//       filename: "ram.jpg",
//     },
//   ];
  
  
//   [
//     "http://localhost:8000/abc.jpg",
//     "http://localhost:8000/nitan.jpg",
//     "http://localhost:8000/ram.jpg",
//   ]

//   let _files = files.map((value,i)=>{
//     return `http://localhost:8000/${value.filename}`

//   })
// console.log(_files)


 //hw
 