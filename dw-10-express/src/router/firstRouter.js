import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/") // localhost:8000/firsts
  .post((req, res,next) => {
    req.name="bidam"
    req.age=21
    req.address="bode"
    next()
  },(req,res)=>{
    console.log(req.name)
    console.log(req.age)
    console.log(req.address)
  })
  
firstRouter
  .route("/name") // localhost:8000/firsts/name
  .post((req, res) => {
    res.json("name post");

  });

firstRouter
  // here we have
  // static route parameter they are firsts,a
  // dynamic  route parameter they are name ,b (in place of dynamic route parameter we can write any)
  .route("/firsts/:name/a/:b") // localhost:8000/firsts/any1/a/any2
  .post((req, res) => {
    console.log(req.params);
    // it gives the dynamic route parameter
    //{name:"nita",b:"1234"}
    res.json("i am first");
  });



export default firstRouter;

