import { Router } from "express";
import { createProduct, deleteProduct, readProduct, readProductDetails, updateProduct } from "../controller/productController.js";

let productRouter = Router()

productRouter
.route("/")
.post(createProduct)
.get(readProduct)

productRouter
.route("/:productId")
.get(readProductDetails)
.patch(updateProduct)
.delete(deleteProduct)


export default productRouter