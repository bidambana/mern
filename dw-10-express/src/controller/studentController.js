import { Student } from "../schema/model.js"

export let createStudent = async(req,res)=>{
    let studentData = req.body
    try {
        let result = await Student.create(studentData)
        res.json({
            success: true,
            message: "student created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }  
}

export let readStudent = async(req,res)=>{
    try {
        let result = await Student.find({})
        res.json({
            success: true,
            message: "students read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

export let readStudentDetails = async(req,res)=>{
    let studentId=req.params.studentId
    try {
        let result = await Student.findById(studentId)
        res.json({
            success:true,
            message:"student read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateStudent = async(req,res)=>{
    let studentId = req.params.studentId
    let studentData = req.body
    try {
        let result = await Student.findByIdAndUpdate(studentId,studentData)
        res.json({
            success:true,
            message:"student updated successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteStudent = async(req,res)=>{
    let studentId = req.params.studentId
    try {
        let result = await Student.findByIdAndDelete(studentId)
        res.json({
            success:true,
            message:"student deleted successfully",
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}