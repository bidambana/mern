import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendemail.js"
import { secretKey } from "../constant.js"
import jwt from "jsonwebtoken"

export let createUser = async(req,res)=>{
    let userData = req.body
    let password = userData.password
    let email = userData.email
    try {
        let hashPassword = await bcrypt.hash(password,10)
        userData.password = hashPassword
        let result = await User.create(userData)

        await sendEmail({
            form:"baby<subekshyakhadka1@gmail.com>",
            to:[email],
            subject:"hi",
            html:`<h1>hehe</h1>`,
            attachments: [
                  {
                    filename: '17023665602821201775.jpg', // Replace with your desired filename
                    path: './public/17023665602821201775.jpg' // Replace with the actual file path on your server
                  }
                ]
        })
        res.status(201).json({
            success: true,
            message: "users created successfully",
            result:result
        })
    } catch (error) {
        res.status(409).json({
            success: false,
            message: error.message
        })
    }    
}

export let readUser = async(req,res)=>{
    let query = req.query
    let brake = query.break
    let page = query.page
    try {
        let result = await User.find(query).skip((page-1)*brake).limit(brake)
        res.status(200).json({
            success: true,
            message: "users read successfully",
            result:result
        })
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        })
        
    }
}

export let readUserDetails = async(req,res)=>{
    let userId=req.params.userId
    try {
        let result = await User.findById(userId)
        res.status(200).json({
            success:true,
            message:"user read successfully",
            result:result
        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }
}

export let updateUser = async(req,res)=>{
    let userId = req.params.userId
    let userData = req.body
    try {
        let result = await User.findByIdAndUpdate(userId,userData)
        res.status(201).json({
            success:true,
            message:"user updated successfully"
        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }
}

export let deleteUser = async(req,res)=>{
    let userId = req.params.userId
    try {
        let result = await User.findByIdAndDelete(userId)
        res.status(200).json({
            success:true,
            message:"user deleted successfully",
        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }
}
export let loginUser = async(req,res)=>{
    let email = req.body.email
    let password = req.body.password
    
    try {
        let user = await User.findOne({email:email})
        let hashPassword = user.password
        if (user===null){
            res.json({
                success:false,
                message:"Email or password does not match"
            })
        } else {
             let isValid = await bcrypt.compare(password,hashPassword)
             if(isValid){
                let infoObj={
                    id:user._id
                }
                let expiryInfo={
                    expiresIn:"365d"
                }
                let token = jwt.sign(infoObj,secretKey,expiryInfo)
                res.json({
                    success:true,
                    message:"User logged in successfully",
                    result:token
                })
             }
             else{
                res.json({
                    success:false,
                    message:"Email or password does not match"
                })
             }
        }
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let myProfile = async(req,res)=>{
    let bearerToken=req.headers.authorization
    let token=bearerToken.split(" ")[1]
    try {
        let infoObj=jwt.verify(token,secretKey)
        let id=infoObj.id
        let result=await User.findById(id)
        res.json({
            success:true,
            message:"Profile read successfully",
            result:result
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}