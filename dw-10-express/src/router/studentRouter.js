import { Router } from "express";
import { createStudent, deleteStudent, readStudent, readStudentDetails, updateStudent } from "../controller/studentController.js";

let studentRouter = Router()

studentRouter
.route("/")
.post(createStudent)
.get(readStudent)

studentRouter
.route("/:studentId")
.get(readStudentDetails)
.patch(updateStudent)
.delete(deleteStudent)


export default studentRouter