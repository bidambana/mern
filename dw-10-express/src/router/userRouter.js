import { Router } from "express";
import { createUser, deleteUser, loginUser, myProfile, readUser, readUserDetails, updateUser } from "../controller/userController.js";

let userRouter = Router()

userRouter
.route("/")
.post(createUser)
.get(readUser)

userRouter
.route("/login")
.post(loginUser)

userRouter
.route("/my-profile")
.get(myProfile)

userRouter
.route("/:userId")
.get(readUserDetails)
.patch(updateUser)
.delete(deleteUser)


export default userRouter