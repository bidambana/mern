import { serverLink } from "../constant.js"

export let handleFile =(req,res)=>{
    let links =req.files.map((value,i)=>{
        return `${serverLink}${value.filename}`
    })
    res.json({
        success:true,
        message:"file uploaded successfully",
        result:links
    })
}