import { Router } from "express";
import { createCollege, deleteCollege, readCollege, readCollegeDetails, updateCollege } from "../controller/collegeController.js";


let collegeRouter = Router()

collegeRouter
.route("/")
.post(createCollege)
.get(readCollege)

collegeRouter
.route("/:collegeId")
.get(readCollegeDetails)
.patch(updateCollege)
.delete(deleteCollege)

export default collegeRouter