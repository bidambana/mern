import { Router } from "express";

let bikeRouter = Router()

bikeRouter
.route("/")
.post(
    (req,res,next)=>{
        console.log("i am middleware 1")
        next("a")
    },
    (err,req,res,next)=>{
        console.log("i am an error middleware 1")
        next()
    },
    (req,res,next)=>{
        console.log("i am middleware 2")
        next()
    },
    (err,req,res,next)=>{
        console.log("i am an error middleware 2")
    },
    (req,res,next)=>{
        console.log("i am middleware 3")
    }
    )


 export default bikeRouter