import { Router } from "express";
import { createWebuser, deleteWebuser, readWebuser, readWebuserDetails, updateWebuser } from "../controller/webuserController.js";

let webuserRouter = Router()

webuserRouter
.route("/")
.post(createWebuser)
.get(readWebuser)

webuserRouter
.route("/:webuserId")
.get(readWebuserDetails)
.patch(updateWebuser)
.delete(deleteWebuser)

export default webuserRouter